CREATE DATABASE Assignment1
USE Assignment1

create table Worker(
WORKER_ID INT primary key,
	FIRST_NAME CHAR(25) NOT NULL,
	LAST_NAME CHAR(25),
	SALARY int check (SALARY between 10000 and 25000),
	JOINING_DATE DATETIME,
	DEPARTMENT CHAR(25) check(DEPARTMENT in('HR','Sales','Accts','IT'))
)

insert into Worker values
(001,'Kanika','Varshney',20000, '2014-02-20 09:00:00','IT'),
(002,'Anusha','Goel',18000, '2014-03-20 08:00:00','HR'),
(003,'Muskan','Dembla',17000, '2014-04-20 10:00:00','Sales'),
(004,'Abhay','Varshney',16000, '2014-05-20 07:00:00','Accts'),
(005,'Akanksha','Yadav',10000, '2014-06-20 06:00:00','HR')

select FIRST_NAME as WORKER_NAME from  Worker 
select UPPER (FIRST_NAME) as WORKER_NAME from  Worker 
select distinct DEPARTMENT from Worker
select  LEFT( FIRST_NAME,3) AS ExtractString from Worker
select FIRST_NAME, charindex ('a',FIRST_NAME) from Worker
--select FIRST_NAME, charindex ('a',FIRST_NAME,2) from Worker
select RTRIM(FIRST_NAME) as fname from Worker
select LTRIM(DEPARTMENT) as dept from Worker
select distinct DEPARTMENT ,LEN(DEPARTMENT) from Worker
select REPLACE(FIRST_NAME,'a','A') from Worker
select CONCAT(TRIM(FIRST_NAME),' ',(LAST_NAME)) as COMPLETE_NAME from Worker
select * from Worker order by FIRST_NAME
select * from Worker order by FIRST_NAME DESC
select * from Worker where FIRST_NAME in('Kanika','Akanksha')
select * from Worker where FIRST_NAME NOT IN('Kanika','Akanksha')
select * from Worker where DEPARTMENT='HR'
select * from Worker where FIRST_NAME like '%a%'
select * from Worker where FIRST_NAME like '%a'
select * from Worker where FIRST_NAME like '_____n'
--select * from Worker where FIRST_NAME like '%n' and len(FIRST_NAME)=6
select * from Worker where salary between 10000 and 18000
select * from Worker where year(JOINING_DATE)=2014 and month(JOINING_DATE)=05
select  count (FIRST_NAME) as totalemployee  FROM Worker  where DEPARTMENT='HR'







